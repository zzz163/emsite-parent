/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.sys.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.common.web.BaseController;
import com.empire.emsite.modules.sys.entity.Log;
import com.empire.emsite.modules.sys.facade.LogFacadeService;

/**
 * 类LogController.java的实现描述：日志Controller
 * 
 * @author arron 2017年10月30日 下午7:23:02
 */
@Controller
@RequestMapping(value = "${adminPath}/sys/log")
public class LogController extends BaseController {

    @Autowired
    private LogFacadeService logFacadeService;

    @RequiresPermissions("sys:log:view")
    @RequestMapping(value = { "list", "" })
    public String list(Log log, HttpServletRequest request, HttpServletResponse response, Model model) {
        Page<Log> page = logFacadeService.findPage(new Page<Log>(request, response), log);
        model.addAttribute("page", page);
        return "modules/sys/logList";
    }

}
